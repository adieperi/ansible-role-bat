# ansible-role-bat
Ansible role for deploying the bat binary

## How to install
### requirements.yml
**Put the file in your roles directory**
```yaml
---
- src: https://gitlab.com/adieperi/ansible-role-bat.git
  scm: git
  version: main
  name: ansible-role-bat
```
### Download the role
```Shell
ansible-galaxy install -f -r ./roles/requirements.yml --roles-path=./roles
```

## Requirements

- Ansible >= 2.10 **(No tests has been realized before this version)**

## Role Variables

All variables which can be overridden are stored in [default/main.yml](default/main.yml) file as well as in table below.

| Name           | Default Value | Choices | Description                        |
| -------------- | ------------- | ------- | -----------------------------------|
| `bat_version` | '0.22.1' | [version](https://github.com/sharkdp/bat/tags) | Choice of the bat version. |
| `bat_force_upgrade` | false| "true / false" | Enable upgrade |

## Example Playbook

```yaml
---
- hosts: all
  tasks:
    - name: Install bat
      include_role:
        name: ansible-role-bat
      vars:
        bat_version: '0.22.0'
        bat_force_upgrade: true
```
## License

This project is licensed under MIT License. See [LICENSE](/LICENSE) for more details.

## Maintainers and Contributors

- [Anthony Dieperink](https://gitlab.com/adieperi)
